# Collection Explorer

.NET Core Console Application

Intended for learning .NET collections and methods as part of module 1 of Noroff Accelerate .NET Fullstack short course.

Demonstrates the use of common .NET collections and exposes the common interfaces and behaviours of these collections to the candidates


## Getting Started

Clone to a local directory.

Open solution in Visual Studio

Run

### Prerequisites

.NET Framework

Visual Studio 2017/19 OR Visual Studio Code


## Authors

***Dean von Schoultz** [deanvons](https://gitlab.com/deanvons)





