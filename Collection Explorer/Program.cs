﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace Collection_Explorer
{
    class Program
    {
        //global input decision variables
        public static int populationChoice = 0, collectionChoice = 0;

        #region Global Collections

        //A list of sample words used for automatic populating
        public static List<string> listOfWords = new List<string>()
        {
            "Humdinger",
            "Supercilious",
            "Lilliputian",
            "Zamboni",
            "Jugulate",
            "Antidisestablishmentarianism",
            "Mellifluous",
            "Defenestrate",
            "Pulchritudinous",
            "Ecdysiast",
            "Discombobulated",
            "Sesquipedalianistic",
            "Prestidigitation",
            "Unctuous",
            "Tumescent",
            "Pecksniffian",
            "Pachycephalosaurus",
            "Duplicitous",
            "Duplicitous"
        };

        //Empty Collection examples 
        public static List<string> listOfWordsEmpty = new List<string>();
        public static ArrayList arrayListOfWords = new ArrayList();
        public static Queue<string> queueOfWords = new Queue<string>();
        public static Stack<string> stackOfWords = new Stack<string>();
        public static Dictionary<int, string> dictionaryOfWords = new Dictionary<int, string>();
        public static HashSet<string> hashSetOfWords = new HashSet<string>();

        #endregion

        static void Main(string[] args)
        {
            //Continuous prompt loop
            do
            {
                Prompt();
            } while (true);
        }

        public static void Prompt()
        {
            PrintHeader();

            //Calls a method which prints the menu to the screen
            PrintOptions();

            //Gets the users number input for how they want to populate a collection
            populationChoice = GetIntegerInput(1, 2);

            //Checks for exit value (-1)
            if (populationChoice == -1)
            {
                Environment.Exit(-1);
            }

            //Calls a method which prints the Collection menu to the screen
            PrintCollectionOptions();

            //Gets the users number input for which collection they want to use
            collectionChoice = GetIntegerInput(1, 6);

            //Checks for how the user want to populate a collection
            if (populationChoice == 1)
            {
                //Checks which collection the user wants to use
                switch (collectionChoice)
                {
                    case 1:
                        //Calls a method which allows the user to populate a List
                        PopulateFromInput(listOfWordsEmpty);
                        PrintCollection(listOfWordsEmpty);
                        break;

                    //Calls a method which allows the user to populate an ArrayList
                    case 2:
                        PopulateFromInput(arrayListOfWords);
                        PrintCollection(arrayListOfWords);
                        break;

                    //Calls a method which allows the user to populate a Queue
                    case 3:
                        PopulateFromInput(queueOfWords);
                        PrintCollection(queueOfWords);
                        break;

                    //Calls a method which allows the user to populate a Stack
                    case 4:
                        PopulateFromInput(stackOfWords);
                        PrintCollection(stackOfWords);
                        break;

                    //Calls a method which allows the user to populate a Dictionary
                    case 5:
                        PopulateFromInput(dictionaryOfWords);
                        PrintCollection(dictionaryOfWords);
                        break;

                    //Calls a method which allows the user to populate a Hashset
                    case 6:
                        PopulateFromInput(hashSetOfWords);
                        PrintCollection(hashSetOfWords);
                        break;

                    //Invalid choice feedback
                    default:
                        Console.WriteLine("Choice invalid");
                        break;
                }
            }

            #region Note - Please Read

            //The Switch statement below contains some statements that look like.... 

            //     someCollection.ForEach(word => someOtherCollection.Add(word));

            //This is the same as (is syntactic sugar for)......

            //     foreach (string word in someCollection)
            //     {
            //         someOtherCollection.Add(word)
            //     }

            #endregion

            //Checks for how the user want to populate a collection
            if (populationChoice == 2)
            {
                //Clears the console of previous output
                Console.Clear();

                //Checks which collection the user wants to use
                switch (collectionChoice)
                {
                    //Calls a method which prints a List of words
                    case 1:
                        PrintCollection(listOfWords);
                        break;

                    //Populates an arrayList with words and prints them to the console
                    case 2:
                        arrayListOfWords.AddRange(listOfWords);
                        PrintCollection(arrayListOfWords);
                        break;

                    //Populates a Queue with words and prints them to the console
                    case 3:
                        listOfWords.ForEach(word => queueOfWords.Enqueue(word));
                        PrintCollection(queueOfWords);
                        break;

                    //Populates a stack with words and prints them to the console
                    case 4:
                        listOfWords.ForEach(word => stackOfWords.Push(word));
                        PrintCollection(stackOfWords);
                        break;

                    //Populates a dictionary with words and prints them to the console
                    case 5:
                        int keyGen = 0;
                        listOfWords.ForEach(word => dictionaryOfWords.Add(keyGen++, word));
                        PrintCollection(dictionaryOfWords);
                        break;

                    //Populates a hashset with words and prints them to the console
                    case 6:
                        listOfWords.ForEach(word => hashSetOfWords.Add(word));
                        PrintCollection(hashSetOfWords);
                        break;

                    //Invalid choice feedback
                    default:
                        Console.WriteLine("Choice invalid");
                        break;
                }
            }
        }

        #region Display Logic

        //Prints population choice options
        public static void PrintOptions()
        {
            Console.WriteLine();
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Welcome to the .NET Collection Explorer");
            Console.WriteLine("---------------------------------------");

            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Please select from the following options by entering the number (Enter -1 to exit)");
            Console.WriteLine("---------------------------------------");

            Console.WriteLine("1 - Populate Collection (Enter your own values)");
            Console.WriteLine("2 - Populated Collection (Already contains values)");
        }

        //Prints Collection selection choices
        public static void PrintCollectionOptions()
        {
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("Please select from the following options by entering the number");
            Console.WriteLine("---------------------------------------");

            Console.WriteLine("1 - List");
            Console.WriteLine("2 - ArrayList");
            Console.WriteLine("3 - Queue");
            Console.WriteLine("4 - Stack");
            Console.WriteLine("5 - Dictionary");
            Console.WriteLine("6 - HashSet");
        }

        //Method overloading is used below to print collections
        public static void PrintCollection(ICollection<string> collection)
        {
            Console.WriteLine();
            Console.WriteLine("---------------------------------------");
            Console.WriteLine(
                $"This {collection.GetType().Name} currently has {collection.Count} strings in it. They are listed below.");
            Console.WriteLine("---------------------------------------");

            foreach (string item in collection)
            {
                Console.WriteLine(item);
            }
        }

        public static void PrintCollection(ArrayList collection)
        {
            Console.WriteLine();
            Console.WriteLine("---------------------------------------");
            Console.WriteLine(
                $"This {collection.GetType().Name} currently has {collection.Count} {collection[0].GetType().Name}s in it. They are listed below.");
            Console.WriteLine("---------------------------------------");

            foreach (string item in collection)
            {
                Console.WriteLine(item);
            }
        }

        public static void PrintCollection(Queue<string> collection)
        {
            Console.WriteLine();
            Console.WriteLine("---------------------------------------");
            Console.WriteLine(
                $"This {collection.GetType().Name} currently has {collection.Count} {collection.Peek().GetType().Name}s in it. They are listed below.");
            Console.WriteLine("---------------------------------------");

            while (collection.Count != 0)
            {
                Console.WriteLine(collection.Dequeue());
            }
        }

        public static void PrintCollection(Stack<string> collection)
        {
            Console.WriteLine();
            Console.WriteLine("---------------------------------------");
            Console.WriteLine(
                $"This {collection.GetType().Name} currently has {collection.Count} {collection.Peek().GetType().Name}s in it. They are listed below.");
            Console.WriteLine("---------------------------------------");

            while (collection.Count != 0)
            {
                Console.WriteLine(collection.Pop());
            }
        }

        public static void PrintCollection(Dictionary<int, string> collection)
        {
            Console.WriteLine();
            Console.WriteLine("---------------------------------------");
            Console.WriteLine(
                $"This {collection.GetType().Name} currently has {collection.Count} {collection[0].GetType().Name} in it. They are listed below.");
            Console.WriteLine("---------------------------------------");

            foreach (var key in collection.Keys)
            {
                Console.WriteLine($"{key}: {collection[key]}");
            }
        }

        public static void PrintHeader()
        {
            string header = @"

   _____      _ _           _   _               ______            _                     
  / ____|    | | |         | | (_)             |  ____|          | |                    
 | |     ___ | | | ___  ___| |_ _  ___  _ __   | |__  __  ___ __ | | ___  _ __ ___ _ __ 
 | |    / _ \| | |/ _ \/ __| __| |/ _ \| '_ \  |  __| \ \/ / '_ \| |/ _ \| '__/ _ \ '__|
 | |___| (_) | | |  __/ (__| |_| | (_) | | | | | |____ >  <| |_) | | (_) | | |  __/ |   
  \_____\___/|_|_|\___|\___|\__|_|\___/|_| |_| |______/_/\_\ .__/|_|\___/|_|  \___|_|   
                                                           | |                          
                                                           |_|                          


";

            Console.WriteLine(header);
        }

        #endregion

        #region Input Logic

        //Provides dynamic integer input prompting
        public static int GetIntegerInput(int min, int max)
        {
            //Prompts user for a number
            Console.WriteLine("Please enter your choice:");

            //Stores user input
            var input = Console.ReadLine();
            int number;

            //Validates user input
            if (!int.TryParse(input, out number))
            {
                Console.WriteLine("Please enter your choice as a single number:");
                number = GetIntegerInput(min, max);
            }

            //Checks for exit value
            if (number == -1)
            {
                return number;
            }

            //Checks for valid input
            if (number >= min && number <= max)
            {
                return number;
            }
            else
            {
                //Error message for invalid input and calls for reprompt
                Console.WriteLine($"Please make sure you input a valid option ({min} - {max}):");
                number = GetIntegerInput(min, max);
            }

            return number;
        }

        //Provides string input prompting
        public static string GetStringInput()
        {
            Console.WriteLine("Please enter a word (Enter 'DONE' to submit the collection for display):");

            string input = Console.ReadLine();

            return input;
        }

        //Method overloading is used below to prompt users to enter words to store in a collection
        public static void PopulateFromInput(ICollection<string> collection)
        {
            string input;

            do
            {
                input = GetStringInput();

                if (input != "DONE")
                {
                    collection.Add(input);
                }
            } while (input != "DONE");
        }

        public static void PopulateFromInput(ArrayList collection)
        {
            string input;

            do
            {
                input = GetStringInput();

                if (input != "DONE")
                {
                    collection.Add(input);
                }
            } while (input != "DONE");
        }

        public static void PopulateFromInput(Queue<string> collection)
        {
            string input;

            do
            {
                input = GetStringInput();

                if (input != "DONE")
                {
                    collection.Enqueue(input);
                }
            } while (input != "DONE");
        }

        public static void PopulateFromInput(Stack<string> collection)
        {
            string input;

            do
            {
                input = GetStringInput();

                if (input != "DONE")
                {
                    collection.Push(input);
                }
            } while (input != "DONE");
        }

        public static void PopulateFromInput(Dictionary<int, string> collection)
        {
            string input;
            int key = 0;

            do
            {
                input = GetStringInput();

                if (input != "DONE")
                {
                    collection.Add(key++, input);
                }
            } while (input != "DONE");
        }

        #endregion

        #region Additional Operations

        //These are random methods to show possible parameter and return type options


        /// <summary>
        /// Displays the number of uppercase or lowercase letters 'A' in a List of words
        /// </summary>
        /// <param name="list">List of words to have the letter 'A' totaled</param>
        public static void CountAsInList(List<string> list)
        {
            //local variable
            int numberOfAs = 0;


            foreach (string word in list)
            {
                for (int i = 0; i < word.Length; i++)
                {
                    if (word[i] == 'a' || word[i] == 'A')
                    {
                        numberOfAs++;
                    }
                }
            }

            Console.WriteLine($"There are:  {numberOfAs} a's in this list");
        }

        /// <summary>
        /// Merges a list of words into a single large word
        /// </summary>
        /// <param name="list">List of word to be merged</param>
        /// <returns>Single merged word</returns>
        public static string ConcatenateList(List<string> list)
        {
            //local variable
            string longestWordEver = "";


            foreach (string word in list)
            {
                longestWordEver += word;
            }


            return longestWordEver;
        }

        /// <summary>
        /// Filters out any words containing the letter 'A'
        /// </summary>
        /// <param name="list">List of words to be filtered</param>
        /// <returns>A list of words containing the letter 'A'</returns>
        public static List<string> GetListOfWordsWithAnA(List<string> list)
        {
            //local variable
            List<string> WordsWithAs = new List<string>();


            foreach (string word in list)
            {
                for (int i = 0; i < word.Length; i++)
                {
                    if (word[i] == 'a' || word[i] == 'A')
                    {
                        WordsWithAs.Add(word);
                        break;
                    }
                }
            }

            return WordsWithAs;
        }

        /// <summary>
        /// Checks whether a could be a palindrome
        /// </summary>
        /// <param name="word">Word to be checked</param>
        /// <returns>True if word could be a palindrome otherwise returns false</returns>
        public static bool IsPossiblePolindrome(string word)
        {
            if (word.EndsWith(word[0]))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Counts the number of times a letter is present in a word
        /// </summary>
        /// <param name="word">Word to search</param>
        /// <param name="letter">Letter to count</param>
        /// <returns></returns>
        public static int CountLetters(string word, char letter)
        {
            //local variable
            int letterTotal = 0;

            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] == letter)
                {
                    letterTotal++;
                }
            }

            return letterTotal;
        }

        #endregion
    }
}
